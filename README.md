## 個人ブログの運用

### 【目的】

プライベートでインフラ運用への理解を深めるために、個人ブログを構築・運用することを目的とした。

### 【実行手法】

#### 1. Conohaサーバを購入

[こちら](https://www.conoha.jp/signup/?btn_id=vps-top--mainvisual_signup&plan=tyo_2gb&ns&service=vps)よりレンタルサーバを契約する。

#### 2. Conohaサーバ購入後にやること

ローカル環境に保存されたAnsibleを対象サーバにSSH経由で流すために、以下の作業が必要となる。

- `ssh-keygen -t edm25519`コマンドで秘密鍵、公開鍵ペアを作成する。
- 秘密鍵はローカルに保存する。
- 公開鍵は`scp`コマンドを利用して、対象サーバに転送する。`scp ~/.ssh/id_rsa.pub <remote machine>`
- 対象サーバへログインし、公開鍵を許可鍵へ登録する。`cat <sUploaded id_rsa.pub> >> ~/.ssh/authorized_keys`
- `.ssh`ディレクトリは`700`, `authorized_keys`ファイルは`600`へ変更する。
- 対象サーバの`/etc/ssh/sshd_config`にて、パスワード認証設定`PasswordAuthentication`を`no`に変更する。
- 上記の設定を反映させるためにsshdを再起動する。`systemctl restart sshd`
- Ansibleが鍵認証ログインを可能にするように、`inventory.yml`にローカルの秘密鍵のパスを登録する。

#### 2.設定情報を追記する

以下のテンプレートファイルを参考に、必要ファイルを作成し、テンプレートファイルが存在するフォルダと同じ階層に配置する。
- [wp-config.php](https://gitlab.com/karasu97/ansible-httpd/-/blob/master/wordpress/wp-config-sample.php)
- [inventory.yml](https://gitlab.com/karasu97/ansible-httpd/-/blob/master/inventory-template.yml)

#### 3. Ansibleを流す

以下のコマンドを入力する。

```
ansible-playbook playbooks/package_install.yml -i inventory.yml
ansible-playbook playbooks/create_user.yml -i inventory.yml
ansible-playbook playbooks/configure_firewall.yml -i inventory.yml
ansible-playbook playbooks/configure_wordpress.yml -i inventory.yml 
ansible-playbook playbooks/configure_sendmail.yml -i inventory.yml
ansible-playbook playbooks/configure_nginx.yml -i inventory.yml
ansible-playbook playbooks/configure_php.yml -i inventory.yml 
ansible-playbook playbooks/configure_db.yml -i inventory.yml
```

#### 4. お名前.comでドメイン名を購入

[こちら](https://www.onamae.com/)よりドメインを契約する。

#### 5. Ansibleを流す

以下のコマンドを入力する。

```
ansible-playbook playbooks/configure_ssl.yml -i inventory.yml
```

#### 6."https://契約したドメイン"にアクセス

### 【技術スタック】

#### レンタルサーバ

- Conoha VPS（ https://www.conoha.jp/vps/pricing ）
  - メモリ: 2GB
  - CPU: 3core
  - SSD: 100GB
  - 初期費用: 無料
  - 月額費用: 1363円

##### Q なぜメモリを2GBを採用したか？
Conoha VPSは1363円/月で一台のサーバをレンタルして、自由にカスタマイズできる点が強みである。今回購入したサーバのスペックは上記の通りである。メモリは512MB, 1GB, 2GBなどから選択できるが、**Wordpressは1GBだと正常に動作する確証が持てなかったこと**、**将来的に監視サービスも同居させたかったため**、ある程度余裕を確保できるメモリが必要と考えた。

##### Q なぜConoha VPSを採用したか？

Conohaはさくら他サービスと比較して、初期費用も少なく、万が一、一週間程度の利用に終わっても、最低利用期間も存在しないため有用だと考えた。[参考](https://blog-bootcamp.jp/start/wordpress-server-vps/#index_id18)

Conoha Wingではなく、VPSを購入した理由として、VPSの方がOSやセキュリティ対策が柔軟に変更できる点にある。今回は運用への学習に軸を置いた構築を目指すため、学習コストは低いが柔軟性の低い、WingよりVPSの方が目的に合致していると考えた。

#### OS

- AlmaLinux

##### Q なぜAlmaLinuxを採用したか？
Linuxはインフラ・カーネル開発・オープンソースソフトウェアに関する深い技術を持っているという点で、インフラの学習するには有用なOSであるため採用した。その中でサポート期間が2029年と先が長く、今後需要が高まってくることも予想されるためAlmaLinuxを採用した。

#### ミドルウェア

- Webサーバ: Nginx 1.20.1
- プログラム: PHP 8.0
- プロセスマネージャ: PHP-FPM 8.0
- DBMS: MySQL 8.0

##### Q なぜApacheではなく、Nginxを採用したか？
一般的にはLAMP環境・WebサーバにApacheを採用しているケースが多いが、諸事情によりNginxを触る機会が多く、Nginx.confの設定などを個人的に勉強したいという思いが勝り、Nginxを採用するに至った。

##### C10K問題
Apacheには[C10K問題](https://www.a-frontier.jp/technology/performance02/)と呼ばれるクライアントが約1万台に達すると、プロセスIDが枯渇し、Webサーバのレスポンスが著しく低下する現象が課題となっている。理由として、HTTPリクエストとプロセスが1対1で対応しているため、プロセスIDの上限以上のリクエストを処理できない。

Nginxはイベント駆動方式であり、プロセスIDの枯渇が発生しない。[参照](https://blog.framinal.life/entry/2015/09/13/195121) そのためC10K問題が発生しないことやレスポンスの速度が早い、などApacheより優位な点はいくつか存在する。

しかし今回はサーバのスペック上、プロセスの上限値を突破するほどのリクエストを同時に受け付ける余裕はないので、考慮対象外であるが、一応頭の片隅に保存しておく。

##### Q なぜCGIを採用したか？
正直、私もわからない...。強いて言えば、情報量が多かったからである。

PHPにはモジュール版とCGI版の二種に大別される。
CGI版ではWebサーバプロセスとアプリケーションプロセスの分離している。その為、相互のプロセス間接続を要するため、レスポンス速度が遅くなりがちである。正直、モジュール版の方が目的に合致している気がしたが、今回はこの点に関する知識は深くなかったため、モジュール版の利用は見合わせることにした。

##### Q なぜPHP-FPMを採用したか？
PHP-FPMとは、PHPのFastCGI実装の一種である。

CGIとは、Webサーバ上でPHPプログラムを動作させるために実装されたインターフェースである。つまり本サービスでは、NginxがPHPファイルを実行させるために必要なインターフェースと考えてもらえれば良い。CGIはユーザから要求がある度に、プロセスの生成と破棄を行う。

FastCGIとは、初回のリクエストに対して起動したプロセスをメモリ上に保存し、次回のリクエストに関しては、メモリ上に保持したプロセスの実行を行う。つまりプロセス生成と破棄の処理を省力化できる。

#### ブログシステム

- Wordpress

##### Q なぜWordpressを採用したか？
圧倒的な情報量が最大の決め手となった。

今回はあくまでサーバ・サービスの運用に焦点を当てた計画であるため、アプリケーションの開発は一切行わない。ブログサービスにはCMS（Content Management System）の一つであるWordpressを活用した。

#### IaC

- Ansible

##### Q なぜAnsibleを採用したか？
インフラ・ミドルウェアのコード管理の必要性は承知していたが、IaCツールの中で最も学習コストが低いためである。ChefやItamaeと比べて、YMLファイルで管理できるAnsibleは初心者にとっては敷居が低いと感じた。

#### ドメイン

- お名前.com

##### Q なぜお名前.comを採用したか？
初期一年間の費用が他サービスより安いため。

#### SSL/TLS証明書認証局

- Let's Encrypt(HTTP-01形式)

##### Q なぜLet's Encryptを採用したか？
Let's EncryptはHTTPSサーバーのセットアップと、ブラウザが信頼する証明書の自動的な取得を、人間の仲介なしに可能である。

